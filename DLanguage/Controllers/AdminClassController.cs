﻿using DLanguage.DataAccess;
using DLanguage.DTO;
using DLanguage.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DLanguage.Controllers
{
    [Route("api/AdminClass")]
    [ApiController]
    public class AdminClassController : ControllerBase
    {
        private readonly AdminClassDataAccess _adminClassDataAccess;
        public AdminClassController(AdminClassDataAccess adminClassDataAccess)
        {
            _adminClassDataAccess = adminClassDataAccess;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var courses = _adminClassDataAccess.GetAll();
            return Ok(courses);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] ClassDTO inputClass)
        {
            if (inputClass.ImageData != null && inputClass.ImageData.Length > 0)
            {
                // Save the file to a temporary location
                var filePath = Path.GetTempFileName();

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await inputClass.ImageFile.CopyToAsync(stream);
                }

                // Read the file as a byte array
                byte[] imageData = System.IO.File.ReadAllBytes(filePath);

                // Create a new Course object with the image data
                Class clas = new Class
                {
                    classId = Guid.NewGuid(),
                    courseId = inputClass.courseId,
                    title = inputClass.title,
                    price = inputClass.price,
                    imgPath = inputClass.imgPath,
                    description = inputClass.description,
                    active = inputClass.active,
                    ImageData = imageData,
                    Created = DateTime.Now,
                    Updated = DateTime.Now
                };

                bool result = _adminClassDataAccess.Insert(clas);

                if (result)
                {
                    return StatusCode(201); // Created
                }
                else
                {
                    return StatusCode(500, "Error occurred while creating the course");
                }
            }
            else
            {
                return BadRequest("Image file is required");
            }
        }

        [HttpPut]
        public IActionResult Put(Guid classId, [FromBody] AdminClassDTO classDto)
        {
            if (classDto == null)
                return BadRequest("Data should be inputed");

            Class clas = new Class
            {
                courseId = classDto.courseId,
                title = classDto.title,
                price = classDto.price,
                imgPath = classDto.imgPath,
                description = classDto.description,
                Updated = DateTime.Now
            };

            bool result = _adminClassDataAccess.Update(classId, clas);

            if (result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [HttpPut("Active")]
        public IActionResult Active([FromQuery]Guid classId)
        {
            bool result = _adminClassDataAccess.Active(classId);

            if (result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

    }
}
