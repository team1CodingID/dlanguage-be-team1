﻿using DLanguage.DataAccess;
using DLanguage.DTO;
using DLanguage.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DLanguage.Controllers
{
    [Route("api/AdminCourse")]
    [ApiController]
    public class AdminCourseController : ControllerBase
    {
        private readonly AdminCourseDataAccess _adminCourseDataAccess;
        public AdminCourseController(AdminCourseDataAccess adminCourseDataAccess)
        {
            _adminCourseDataAccess = adminCourseDataAccess;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var courses = _adminCourseDataAccess.GetAll();
            return Ok(courses);
        }

        [HttpPost]
        public IActionResult Post([FromBody] CourseDTO courseDto)
        {
            if (courseDto == null)
                return BadRequest("Data should be inputed");

            Course course = new Course
            {
                courseId = Guid.NewGuid(),
                courseName = courseDto.courseName,
                description = courseDto.description,
                coursePath = courseDto.coursePath,
                active = courseDto.active,
                Created = DateTime.Now,
                Updated = DateTime.Now
            };

            bool result = _adminCourseDataAccess.Insert(course);

            if (result)
            {
                return StatusCode(201, course.courseId);
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [HttpPut("Edit")]
        public async Task<IActionResult> Put(Guid courseId, [FromForm] EditCourseDTO courseDto)
        {
            try
            {
                if (courseDto == null)
                {
                    return BadRequest("Data should be inputted");
                }

                EditCourseDTO course = new EditCourseDTO
                {
                    courseName = courseDto.courseName,
                    description = courseDto.description,
                    coursePath = courseDto.coursePath,
                };

                bool result = _adminCourseDataAccess.Update(courseId, course);

                if (result)
                {
                    return StatusCode(201); // Created
                }
                else
                {
                    return StatusCode(500, "Error occurred while creating the course");
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);

                return StatusCode(500, "An internal server error occurred");
            }
        }

        [HttpPut("Active")]
        public IActionResult Active(Guid courseId)
        {
            bool result = _adminCourseDataAccess.Active(courseId);

            if (result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

    }
}
