﻿using DLanguage.DataAccess;
using DLanguage.DTO;
using DLanguage.DTO.Customer;
using DLanguage.Emails;
using DLanguage.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace DLanguage.Controllers
{
    [Route("api/AdminCustomer")]
    [ApiController]
    public class AdminCustomerController : ControllerBase
    {
        private readonly AdminCustomerDataAccess _userData;
        private readonly IConfiguration _configuration;

        public AdminCustomerController(AdminCustomerDataAccess userData, IConfiguration configuration)
        {
            _userData = userData;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var courses = _userData.GetCustomerList();
            return Ok(courses);
        }

        [HttpPost("CreateUser")]
        public async Task<IActionResult> CreateUser([FromBody] CustomerDTO userDto)
        {
            try
            {
                Customer user = new Customer
                {
                    customerId = Guid.NewGuid(),
                    customerName = userDto.customerName,
                    email = userDto.email,
                    password = BCrypt.Net.BCrypt.HashPassword(userDto.password),
                    isActivated = false
                };

                UserRole userRole = new UserRole
                {
                    customerId = user.customerId,
                    role = userDto.role
                };

                bool result = _userData.CreateUserAccount(user, userRole);

                if (result)
                {
                    return StatusCode(201, userDto);
                }
                else
                {
                    return StatusCode(500, "Data not inserted");
                }
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Put(Guid customerId, [FromBody] CustomerDTO customerDto)
        {
            if (customerDto == null)
                return BadRequest("Data should be inputed");

            CustomerDTO customer = new CustomerDTO
            {
                customerName = customerDto.customerName,
                email = customerDto.email,
                password = BCrypt.Net.BCrypt.HashPassword(customerDto.password),
                role = customerDto.role
            };

            bool result = _userData.Update(customerId, customer);

            if (result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [HttpPut("Active")]
        public IActionResult Active(Guid customerId)
        {
            bool result = _userData.Active(customerId);

            if (result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }
    }
}
