﻿using DLanguage.DataAccess;
using DLanguage.DTO;
using DLanguage.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DLanguage.Controllers
{
    [Route("api/AdminMethod")]
    [ApiController]
    public class AdminCMethodController : ControllerBase
    {
        private readonly AdminMethodDataAccess _adminMethodDataAccess;
        public AdminCMethodController(AdminMethodDataAccess adminMethodDataAccess)
        {
            _adminMethodDataAccess = adminMethodDataAccess;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var courses = _adminMethodDataAccess.GetAll();
            return Ok(courses);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] MethodDTO method)
        {
            if (method == null)
                return BadRequest("Data should be inputed");

            if (method.ImageData != null && method.ImageData.Length > 0)
            {
                // Save the file to a temporary location
                var filePath = Path.GetTempFileName();

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await method.ImageFile.CopyToAsync(stream);
                }

                // Read the file as a byte array
                byte[] imageData = System.IO.File.ReadAllBytes(filePath);

                // Create a new Course object with the image data
                MethodDTO methodInstance = new MethodDTO
                {
                    methodId = Guid.NewGuid(),
                    methodName = method.methodName,
                    methodPath = method.methodPath,
                    ImageData = imageData
                };

                bool result = _adminMethodDataAccess.Insert(methodInstance);

                if (result)
                {
                    return StatusCode(201); // Created
                }
                else
                {
                    return StatusCode(500, "Error occurred while creating the course");
                }
            }
            else
            {
                return BadRequest("Image file is required");
            }
        }

        [HttpPut]
        public IActionResult Put(Guid methodId, [FromBody] EditMethodDTO methodDTO)
        {
            if (methodDTO == null)
                return BadRequest("Data should be inputed");

            EditMethodDTO instance = new EditMethodDTO
            {
                methodName = methodDTO.methodName,
                methodPath = methodDTO.methodPath,
            };

            bool result = _adminMethodDataAccess.Update(methodId, instance);

            if (result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [HttpPut("Active")]
        public IActionResult Active(Guid methodId)
        {
            bool result = _adminMethodDataAccess.Active(methodId);

            if (result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [HttpGet("GetUser")]
        public IActionResult GetAllCustomer()
        {
            var courses = _adminMethodDataAccess.GetAllCustomer();
            return Ok(courses);
        }

    }
}
