﻿using DLanguage.DataAccess;
using DLanguage.DTO;
using DLanguage.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DLanguage.Controllers
{
    [Route("api/class")]
    [ApiController]
    public class ClassController : ControllerBase
    {
        private readonly ClassDataAccess _classDataAccess;
        public ClassController(ClassDataAccess classDataAccess)
        {
            _classDataAccess = classDataAccess;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var classes = _classDataAccess.GetAll();
            return Ok(classes);
        }

        [HttpGet("GetById")]
        public IActionResult Get(Guid classId)
        {
            ClassDTO? clas = _classDataAccess.GetById(classId);

            if (clas == null)
            {
                return NotFound("Data not found");
            }

            return Ok(clas);
        }

        [HttpGet("GetByCategory")]
        public IActionResult GetByCategory(Guid courseId)
        {
            var clas = _classDataAccess.GetByCategory(courseId);

            if (clas == null)
            {
                return NotFound("Data not found");
            }

            return Ok(clas);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Class inputClass)
        {
            if (inputClass == null)
                return BadRequest("Data should be inputed");

            Class clas = new Class
            {
                classId = Guid.NewGuid(),
                courseId = inputClass.courseId,
                title = inputClass.title,
                price = inputClass.price,
                imgPath = inputClass.imgPath,
                description = inputClass.description,
                Created = DateTime.Now,
                Updated = DateTime.Now
            };

            bool result = _classDataAccess.Insert(clas);

            if (result)
            {
                return StatusCode(201, clas.classId);
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [HttpPut]
        public IActionResult Put(Guid classId, [FromBody] ClassDTO classDto)
        {
            if (classDto == null)
                return BadRequest("Data should be inputed");

            Class clas = new Class
            {
                title = classDto.title,
                price = classDto.price,
                imgPath = classDto.imgPath,
                description = classDto.description,
                Updated = DateTime.Now
            };

            bool result = _classDataAccess.Update(classId, clas);

            if (result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [HttpDelete]
        public IActionResult Delete(Guid classId)
        {
            bool result = _classDataAccess.Delete(classId);

            if (result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }
    }
}
