﻿using DLanguage.DataAccess;
using DLanguage.DTO;
using DLanguage.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;

namespace DLanguage.Controllers
{
    [Route("api/course")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly CourseDataAccess _courseDataAccess;
        private readonly IWebHostEnvironment _environment;
        public CourseController(CourseDataAccess courseDataAccess, IWebHostEnvironment environment)
        {
            _courseDataAccess = courseDataAccess;
            _environment = environment;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var courses = _courseDataAccess.GetAll();
            return Ok(courses);
        }

        [HttpGet("GetById")]
        public IActionResult Get(Guid courseId)
        {
            Course? course = _courseDataAccess.GetById(courseId);

            if (course == null)
            {
                return NotFound("Data not found");
            }

            return Ok(course);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] CourseDTO courseDto)
        {
            try
            {
                if (courseDto == null)
                {
                    return BadRequest("Data should be inputted");
                }

                if (courseDto.ImageData != null && courseDto.ImageData.Length > 0)
                {
                    // Save the file to a temporary location
                    var filePath = Path.GetTempFileName();

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await courseDto.ImageFile.CopyToAsync(stream);
                    }

                    // Read the file as a byte array
                    byte[] imageData = System.IO.File.ReadAllBytes(filePath);

                    // Create a new Course object with the image data
                    CourseDTO course = new CourseDTO
                    {
                        courseName = courseDto.courseName,
                        description = courseDto.description,
                        coursePath = courseDto.coursePath,
                        active = courseDto.active,
                        ImageData = imageData // Store the image data in the DTO
                    };

                    bool result = _courseDataAccess.Insert(course);

                    if (result)
                    {
                        return StatusCode(201); // Created
                    }
                    else
                    {
                        return StatusCode(500, "Error occurred while creating the course");
                    }
                }
                else
                {
                    return BadRequest("Image file is required");
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);

                return StatusCode(500, "An internal server error occurred");
            }
        }



        [HttpPut]
        public IActionResult Put(Guid courseId, [FromBody] CourseDTO courseDto)
        {
            if (courseDto == null)
                return BadRequest("Data should be inputed");

            Course course = new Course
            {
                courseName = courseDto.courseName,
                description = courseDto.description,
                coursePath = courseDto.coursePath,
                Updated = DateTime.Now,
            };

            bool result = _courseDataAccess.Update(courseId, course);

            if (result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [HttpDelete]
        public IActionResult Delete(Guid courseId)
        {
            bool result = _courseDataAccess.Delete(courseId);

            if (result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [NonAction]
        public async Task<string> SaveImage(IFormFile imageFile)
        {
            string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
            var imagePath = Path.Combine(_environment.ContentRootPath, "Images", imageName);
            using (var fileStream = new FileStream(imagePath, FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }
            return imageName;
        }

    }
}
