﻿using DLanguage.DataAccess;
using DLanguage.DTO.Customer;
using DLanguage.Emails;
using DLanguage.Emails.Template;
using DLanguage.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Text.Json;

namespace DLanguage.Controllers
{
    [Route("api/User")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerDataAccess _userData;
        private readonly IConfiguration _configuration;
        private readonly EmailService _emailService;

        public CustomerController(CustomerDataAccess userData, IConfiguration configuration, EmailService emailService)
        {
            _userData = userData;
            _configuration = configuration;
            _emailService = emailService;
        }

        [HttpPost("CreateUser")]
        public async Task<IActionResult> CreateUser([FromBody] CustomerDTO userDto)
        {
            try
            {
                Customer user = new Customer
                {
                    customerId = Guid.NewGuid(),
                    customerName = userDto.customerName,
                    email = userDto.email,
                    password = BCrypt.Net.BCrypt.HashPassword(userDto.password),
                    isActivated = false
                };

                UserRole userRole = new UserRole
                {
                    customerId = user.customerId,
                    role = userDto.role
                };

                bool result = _userData.CreateUserAccount(user, userRole);

                if (result)
                {
                    bool mailResult = await SendMailActivation(user);
                    return StatusCode(201, userDto);
                }
                else
                {
                    return StatusCode(500, "Data not inserted");
                }
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginRequestDTO credential)
        {
            if (credential is null)
                return BadRequest("Invalid client request");

            if (string.IsNullOrEmpty(credential.email) || string.IsNullOrEmpty(credential.password))
                return BadRequest("Invalid client request");

            Customer? user = _userData.CheckUser(credential.email);

            if (user == null)
                return Unauthorized("You do not authorized");

            if (user.isActivated != true)
            {
                return Unauthorized("Your account has not activated");
            }

            UserRole? userRole = _userData.GetUserRole(user.customerId);


            bool isVerified = BCrypt.Net.BCrypt.Verify(credential.password, user.password);

            if (user != null && !isVerified)
            {
                return BadRequest("Incorrect Password! Please check your password!");
            }
            else if (user != null && isVerified && userRole?.role == "Admin")
            {
                return Ok(new AdminResponseDTO { customerId = user.customerId, userRole = userRole.role });
            }
            else if (user != null && isVerified && userRole?.role == "User")
            {
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                _configuration.GetSection("JwtConfig:Key").Value));

                var claims = new Claim[] {
                    new Claim(ClaimTypes.Email, user.email),
                    new Claim(ClaimTypes.Role, userRole.role)
                };

                var signingCredential = new SigningCredentials(
                    key, SecurityAlgorithms.HmacSha256Signature);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.UtcNow.AddMinutes(10),
                    SigningCredentials = signingCredential
                };

                var tokenHandler = new JwtSecurityTokenHandler();

                var securityToken = tokenHandler.CreateToken(tokenDescriptor);

                string token = tokenHandler.WriteToken(securityToken);

                return Ok(new LoginResponseDTO { Token = token, customerId = user.customerId, userRole = userRole.role });
            }
            else
            {
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                _configuration.GetSection("JwtConfig:Key").Value));

                var claims = new Claim[] {
                    new Claim(ClaimTypes.Email, user.email),
                    new Claim(ClaimTypes.Role, userRole.role)
                };

                var signingCredential = new SigningCredentials(
                    key, SecurityAlgorithms.HmacSha256Signature);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.UtcNow.AddMinutes(10),
                    SigningCredentials = signingCredential
                };

                var tokenHandler = new JwtSecurityTokenHandler();

                var securityToken = tokenHandler.CreateToken(tokenDescriptor);

                string token = tokenHandler.WriteToken(securityToken);

                return Ok(new LoginResponseDTO { Token = token, customerId = user.customerId, userRole = userRole.role });
            }
        }

        [HttpGet("ActivateUser")]
        public IActionResult ActivateUser(Guid customerId, string email)
        {
            try
            {
                Customer? user = _userData.CheckUser(email);

                if (user == null)
                    return BadRequest("Activation Failed");

                if (user.isActivated == true)
                    return BadRequest("Account has been activated");

                bool result = _userData.AcitvateUser(customerId);

                if (result)
                    return Ok("User Activated Successfully");
                else
                    return BadRequest("Activation Failed");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("SendMailUser")]
        public async Task<IActionResult> SendMailUser([FromBody] string mailTo)
        {
            List<string> to = new List<string>();
            to.Add(mailTo);

            string subject = "Test Email FS10";
            string body = "Hallo, First Email";

            EmailModel model = new EmailModel(to, subject, body);

            bool sendMail = await _emailService.SendAsync(model, new CancellationToken());

            if (sendMail)
                return Ok("Send");
            else
                return StatusCode(500, "Error");
        }



        private async Task<bool> SendMailActivation(Customer user)
        {
            if (user == null)
                return false;

            if (string.IsNullOrEmpty(user.email))
                return false;

            List<string> to = new List<string>();
            to.Add(user.email);

            string subject = "Account Activation";

            var param = new Dictionary<string, string>()
            {
                {"customerId", user.customerId.ToString() },
                { "email", user.email }
            };

            string callback = QueryHelpers.AddQueryString("https://localhost:7171/api/User/ActivateUser", param);

            //string body = "Please confirm account by clicking this <a href=\"" + callback + "\"> Link</a>";

            string body = _emailService.GetMailTemplate("EmailActivation", new ActivationModel()
            {
                Email = user.email,
                Link = callback
            });

            EmailModel emailModel = new EmailModel(to, subject, body);
            bool mailResult = await _emailService.SendAsync(emailModel, new CancellationToken());

            return mailResult;
        }

        [HttpPost("ForgetPassword")]
        public async Task<IActionResult> ForgetPassword([FromBody] string email)
        {
            try
            {
                string? mail = email.ToString();
                if (string.IsNullOrEmpty(mail))
                    return BadRequest("Email is empty");

                bool sendMail = await SendEmailForgetPassword(mail);

                if (sendMail)
                {
                    return Ok("Mail sent");
                }
                else
                {
                    return StatusCode(500, "Error");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        [HttpPost("ResetPassword")]
        public IActionResult ResetPassword([FromBody] ResetPasswordDTO resetPassword)
        {
            try
            {
                if (resetPassword == null)
                    return BadRequest("No Data");

                if (resetPassword.Password != resetPassword.ConfirmPassword)
                {
                    return BadRequest("Password doesn't match");
                }

                bool reset = _userData.ResetPassword(resetPassword.Email, BCrypt.Net.BCrypt.HashPassword(resetPassword.Password));

                if (reset)
                {
                    return Ok("Reset password OK");
                }
                else
                {
                    return StatusCode(500, "Error");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        private async Task<bool> SendEmailForgetPassword(string email)
        {
            // send email
            List<string> to = new List<string>();
            to.Add(email);

            string subject = "Forget Password";

            var param = new Dictionary<string, string?>
                    {
                        {"email", email }
                    };

            string callbackUrl = "http://localhost:3000/resetPassword/" + param["email"];

            string body = "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>";

            EmailModel mailModel = new EmailModel(to, subject, body);

            bool mailResult = await _emailService.SendAsync(mailModel, new CancellationToken());

            return mailResult;

        }

    }
}
