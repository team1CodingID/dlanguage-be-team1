﻿using DLanguage.DataAccess;
using DLanguage.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DLanguage.Controllers
{
    [Route("api/detailOrder")]
    [ApiController]
    public class DetailController : ControllerBase
    {
        private readonly DetailDataAccess _detailDataAccess;

        public DetailController (DetailDataAccess detailDataAccess)
        {
            _detailDataAccess = detailDataAccess;
        }

        [HttpGet]
        public IActionResult GetAll(Guid customerId)
        {
            var detail = _detailDataAccess.GetAll(customerId);
            return Ok(detail);
        }

        [HttpPut]
        public IActionResult Keranjang(Guid classId, Guid customerId, [FromBody] string scheduleDate)
        {
            DateOnly date = DateOnly.Parse(scheduleDate);
            
            bool result = _detailDataAccess.Keranjang(classId, customerId, date);

            if (result)
            {
                return StatusCode(201, "Success Added to Cart");
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [HttpDelete]
        public IActionResult Delete(Guid detailId)
        {
            bool result = _detailDataAccess.Delete(detailId);

            if (result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }
    }
}
