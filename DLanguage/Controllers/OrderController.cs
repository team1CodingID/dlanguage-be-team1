﻿using DLanguage.DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DLanguage.Controllers
{
    [Route("api/order")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly OrderDataAccess _orderDataAccess;

        public OrderController(OrderDataAccess orderDataAccess)
        {
            _orderDataAccess = orderDataAccess;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var classes = _orderDataAccess.GetAll();
            return Ok(classes);
        }

        [HttpPut]
        public IActionResult Checkout(Guid customerId)
        {
            bool result = _orderDataAccess.Checkout(customerId);

            if(result)
            {
                return NoContent();
            }
            else
            {
                return StatusCode(500, "Error occur");
            }
        }

        [HttpGet("invoice")]
        public IActionResult GetInvoiceUser(Guid customerId)
        {
            var invoice = _orderDataAccess.GetInvoiceUser(customerId);
            return Ok(invoice);
        }

        [HttpGet("detailInvoice")]
        public IActionResult GetDetailInvoice(Guid orderId)
        {
            var invoice = _orderDataAccess.GetDetailInvoice(orderId);
            return Ok(invoice);
        }

        [HttpGet("myClass")]
        public IActionResult MyClass(Guid customerId)
        {
            var myClass = _orderDataAccess.MyClass(customerId);
            return Ok(myClass);
        }

        [HttpGet("invoiceAdmin")]
        public IActionResult GetInvoiceAdmin()
        {
            var invoice = _orderDataAccess.GetInvoiceAdmin();
            return Ok(invoice);
        }
    }
}
