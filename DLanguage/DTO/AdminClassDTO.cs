﻿namespace DLanguage.DTO
{
    public class AdminClassDTO
    {
        public Guid courseId { get; set; }
        public string title { get; set; } = string.Empty;
        public decimal price { get; set; }
        public string imgPath { get; set; } = string.Empty;
        public string description { get; set; } = string.Empty;
    }
}
