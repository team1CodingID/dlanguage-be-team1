﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DLanguage.DTO
{
    public class CourseDTO
    {
        public Guid courseId { get; set; }
        public string courseName { get; set; } = string.Empty;
        public string description { get; set; } = string.Empty;
        public string coursePath { get; set; } = string.Empty;
        public bool active { get; set; }
        public IFormFile ImageFile { get; set; }
        public byte[] ImageData { get; set; }
    }
}
