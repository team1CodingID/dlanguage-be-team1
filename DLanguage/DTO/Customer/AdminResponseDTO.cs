﻿namespace DLanguage.DTO.Customer
{
    public class AdminResponseDTO
    {
        public Guid customerId { get; set; }
        public string userRole { get; set; } = string.Empty;
    }
}
