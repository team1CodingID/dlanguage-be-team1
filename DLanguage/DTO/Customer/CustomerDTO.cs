﻿namespace DLanguage.DTO.Customer
{
    public class CustomerDTO
    {
        public Guid customerId { get; set; }
        public string customerName { get; set; } = string.Empty;
        public string email { get; set; } = string.Empty;
        public string password { get; set; } = string.Empty;
        public string role { get; set; } = string.Empty;
    }
}
