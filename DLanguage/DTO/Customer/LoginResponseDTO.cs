﻿namespace DLanguage.DTO.Customer
{
    public class LoginResponseDTO
    {
        public string Token { get; set; } = string.Empty;

        public Guid customerId { get; set; }
        public string userRole { get; set; } = string.Empty;
    }
}
