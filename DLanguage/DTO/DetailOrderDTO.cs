﻿namespace DLanguage.DTO
{
    public class DetailOrderDTO
    {
        public Guid detailId { get; set; }
        public string invoiceCode { get; set; } = string.Empty;
        public DateTime orderDate { get; set; }
        public decimal totalPrice { get; set; }
        public string classTitle { get; set; } = string.Empty;
        public string courseName { get; set; } = string.Empty;
        public DateTime scheduleDate { get; set; }
        public decimal price { get; set; }
        public byte[] ImageData { get; set; }
    }
}
