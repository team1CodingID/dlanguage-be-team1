﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DLanguage.DTO
{
    public class EditCourseDTO
    {
        public string courseName { get; set; } = string.Empty;
        public string description { get; set; } = string.Empty;
        public string coursePath { get; set; } = string.Empty;
    }
}
