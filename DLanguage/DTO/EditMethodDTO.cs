﻿namespace DLanguage.Models
{
    public class EditMethodDTO
    {
        public string methodName { get; set; } = string.Empty;
        public string methodPath { get; set; } = string.Empty;
    }
}
