﻿namespace DLanguage.Models
{
    public class MethodDTO
    {
        public Guid methodId { get; set; }
        public string methodName { get; set; } = string.Empty;
        public string methodPath { get; set; } = string.Empty;
        public byte[] ImageData { get; set; }
        public IFormFile ImageFile { get; set; }
    }
}
