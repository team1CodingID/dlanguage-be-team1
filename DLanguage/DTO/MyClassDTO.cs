﻿namespace DLanguage.DTO
{
    public class MyClassDTO
    {
        public string courseName { get; set; } = string.Empty;
        public string classTitle { get; set; } = string.Empty;
        public DateTime scheduleDate { get; set; }
        public string imgPath { get; set; } = string.Empty;
        public byte[] ImageData { get; set; }
    }
}
