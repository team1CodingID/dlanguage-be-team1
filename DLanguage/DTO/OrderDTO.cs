﻿namespace DLanguage.DTO
{
    public class OrderDTO
    {
        public Guid orderId { get; set; }
        public string invoiceCode { get; set; } = string.Empty;
        public DateTime orderDate { get; set; }
        public int totalCourse { get; set; }
        public decimal totalPrice { get; set; }
    }
}
