﻿using DLanguage.DTO;
using DLanguage.Models;
using MySql.Data.MySqlClient;

namespace DLanguage.DataAccess
{
    public class AdminCourseDataAccess
    {
        private readonly string _connectionString;
        private readonly IConfiguration _configuration;

        public AdminCourseDataAccess(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public List<Course> GetAll()
        {
            List<Course> list = new List<Course>();

            using(MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using(MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        connection.Open();

                        command.Connection = connection;
                        command.CommandText = "SELECT * FROM course ORDER BY courseName";

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                list.Add(new Course
                                {
                                    courseId = Guid.Parse(reader["courseId"].ToString() ?? string.Empty),
                                    courseName = reader["courseName"].ToString() ?? string.Empty,
                                    description = reader["description"].ToString() ?? string.Empty,
                                    coursePath = reader["coursePath"].ToString() ?? string.Empty,
                                    active = Convert.ToBoolean(reader["active"]),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return list;
        }

        public bool Insert(Course course)
        {
            bool result = false;

            string created = course.Created.Date.ToString("yyyy-MM-dd HH:mm:ss");
            string updated = course.Updated.Date.ToString("yyyy-MM-dd HH:mm:ss");

            string query = $"INSERT INTO course (courseId, courseName, description, coursePath, active, Created, Updated)" +
                $"VALUES (@id,@courseName,@description,@coursePath,@active,@created,@updated)";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@id", course.courseId);
                        command.Parameters.AddWithValue("@courseName", course.courseName);
                        command.Parameters.AddWithValue("@description", course.description);
                        command.Parameters.AddWithValue("@coursePath", course.coursePath);
                        command.Parameters.AddWithValue("@active", course.active);
                        command.Parameters.AddWithValue("@created", created);
                        command.Parameters.AddWithValue("@updated", updated);

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

        public bool Update(Guid courseId, EditCourseDTO course)
        {
            bool result = false;

            string updated = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string query = $"UPDATE course SET courseName = @courseName, description = @description, coursePath = @coursePath, Updated = @Updated " +
                $"WHERE courseId = @id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@id", courseId);
                        command.Parameters.AddWithValue("@courseName", course.courseName);
                        command.Parameters.AddWithValue("@description", course.description);
                        command.Parameters.AddWithValue("@coursePath", course.coursePath);
                        command.Parameters.AddWithValue("@Updated", updated);

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

        public bool Active(Guid courseId)
        {
            bool result = false;

            string updated = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string query = "UPDATE course SET active = !active, updated = @updated WHERE courseId = @id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@id", courseId);
                        command.Parameters.AddWithValue("@Updated", updated);

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }
    }
}
