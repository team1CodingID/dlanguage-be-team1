﻿using DLanguage.DTO.Customer;
using DLanguage.Models;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace DLanguage.DataAccess
{
    public class AdminCustomerDataAccess
    {
        private readonly string _connectionString;
        private readonly IConfiguration _configuration;

        public AdminCustomerDataAccess(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public List<CustomerDTO> GetCustomerList()
        {
            List<CustomerDTO> result = new List<CustomerDTO>();

            using(MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using(MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        connection.Open();

                        cmd.Connection = connection;
                        cmd.CommandText = "SELECT * FROM customer INNER JOIN userrole ON customer.customerId = userrole.customerId ORDER BY customer.customerName";

                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result.Add(new CustomerDTO
                                {
                                    customerId = Guid.Parse(reader["customerId"].ToString() ?? string.Empty),
                                    customerName = reader["customerName"].ToString() ?? string.Empty,
                                    email = reader["email"].ToString() ?? string.Empty,
                                    role = reader["role"].ToString() ?? string.Empty
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

        public bool CreateUserAccount(Customer user, UserRole userRole)
        {
            bool result = false;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                // open connection before begin transaction
                connection.Open();

                MySqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    DateTime date = DateTime.Now;

                    MySqlCommand command1 = new MySqlCommand();
                    command1.Connection = connection;
                    command1.Transaction = transaction;
                    command1.Parameters.Clear();

                    command1.CommandText = "INSERT INTO customer (customerId, customerName,email,password,isActivated,created,updated) VALUES (@customerId,@customerName,@email,@password,@isActivated,@created,@updated)";
                    command1.Parameters.AddWithValue("@customerId", user.customerId);
                    command1.Parameters.AddWithValue("@customerName", user.customerName);
                    command1.Parameters.AddWithValue("@email", user.email);
                    command1.Parameters.AddWithValue("@password", user.password);
                    command1.Parameters.AddWithValue("@isActivated", user.isActivated);
                    command1.Parameters.AddWithValue("@created", date);
                    command1.Parameters.AddWithValue("@updated", date);


                    MySqlCommand command2 = new MySqlCommand();
                    command2.Connection = connection;
                    command2.Transaction = transaction;
                    command2.Parameters.Clear();

                    command2.CommandText = "INSERT INTO userrole (customerId, role, created, updated) VALUES (@customerId, @role, @created, @updated)";
                    command2.Parameters.AddWithValue("@customerId", userRole.customerId);
                    command2.Parameters.AddWithValue("@role", userRole.role);
                    command2.Parameters.AddWithValue("@created", date);
                    command2.Parameters.AddWithValue("@updated", date);


                    var result1 = command1.ExecuteNonQuery();
                    var result2 = command2.ExecuteNonQuery();

                    transaction.Commit();

                    result = true;
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }

            }

            return result;
        }

        public bool Update(Guid customerId, CustomerDTO customer)
        {
            bool result = false;

            string updated = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string query = $"UPDATE customer SET customerName = @customerName, email = @email, password = @password, Updated = @Updated " +
                $"WHERE customerId = @id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@id", customerId);
                        command.Parameters.AddWithValue("@customerName", customer.customerName);
                        command.Parameters.AddWithValue("@email", customer.email);
                        command.Parameters.AddWithValue("@password", customer.password);
                        command.Parameters.AddWithValue("@Updated", updated);

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

        public bool Active(Guid customerId)
        {
            bool result = false;

            string updated = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string query = "UPDATE customer SET isActivated = !isActivated, updated = @updated WHERE customerId = @id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@id", customerId);
                        command.Parameters.AddWithValue("@Updated", updated);

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }
    }
}
