﻿using DLanguage.DTO.Customer;
using DLanguage.Models;
using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Common;
using System.Collections.Generic;

namespace DLanguage.DataAccess
{
    public class AdminMethodDataAccess
    {
        private readonly string _connectionString;
        private readonly IConfiguration _configuration;

        public AdminMethodDataAccess(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public List<Method> GetAll()
        {
            List<Method> result = new List<Method>();

            using(MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using(MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        connection.Open();

                        cmd.Connection = connection;
                        cmd.CommandText = "SELECT * FROM paymentmethod ORDER BY methodName";

                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result.Add(new Method
                                {
                                    methodId = Guid.Parse(reader["methodId"].ToString() ?? string.Empty),
                                    methodName = reader["methodName"].ToString() ?? string.Empty,
                                    methodPath = reader["methodPath"].ToString() ?? string.Empty,
                                    active = Convert.ToBoolean(reader["active"])
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

        public bool Insert(MethodDTO method)
        {
            bool result = false;
            
            string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string query = $"INSERT INTO paymentmethod (methodId, methodName, methodPath, active, imageData, Created, Updated) " +
                $"VALUES (@id,@methodName,@methodPath,@active,@imageData,@created,@updated)";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@id", method.methodId);
                        command.Parameters.AddWithValue("@methodName", method.methodName);
                        command.Parameters.AddWithValue("@methodPath", method.methodPath);
                        command.Parameters.AddWithValue("@active", false);
                        command.Parameters.AddWithValue("@imageData", method.ImageData);
                        command.Parameters.AddWithValue("@created", date);
                        command.Parameters.AddWithValue("@updated", date);
                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

        public bool Update(Guid methodId, EditMethodDTO method)
        {
            bool result = false;

            string updated = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string query = $"UPDATE paymentmethod SET methodName = @methodName, methodPath = @methodPath, Updated = @Updated " +
                $"WHERE methodId = @id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@id", methodId);
                        command.Parameters.AddWithValue("@methodName", method.methodName);
                        command.Parameters.AddWithValue("@methodPath", method.methodPath);
                        command.Parameters.AddWithValue("@Updated", updated);

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

        public bool Active(Guid methodId)
        {
            bool result = false;

            string updated = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string query = "UPDATE paymentmethod SET active = !active, updated = @updated WHERE methodId = @id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@id", methodId);
                        command.Parameters.AddWithValue("@Updated", updated);

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

        public List<MethodDTO> GetAllCustomer()
        {
            List<MethodDTO> result = new List<MethodDTO>();

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        connection.Open();

                        cmd.Connection = connection;
                        cmd.CommandText = "SELECT * FROM paymentmethod WHERE active = 1";

                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result.Add(new MethodDTO
                                {
                                    methodId = Guid.Parse(reader["methodId"].ToString() ?? string.Empty),
                                    methodName = reader["methodName"].ToString() ?? string.Empty,
                                    methodPath = reader["methodPath"].ToString() ?? string.Empty,
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }
    }
}
