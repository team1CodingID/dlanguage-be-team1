﻿using DLanguage.DTO;
using DLanguage.Models;
using MySql.Data.MySqlClient;

namespace DLanguage.DataAccess
{
    public class ClassDataAccess
    {
        private readonly string _connectionString;
        private readonly IConfiguration _configuration;

        public ClassDataAccess(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public List<ClassDTO> GetAll()
        {
            List<ClassDTO> classes = new List<ClassDTO>();

            string query = "SELECT * FROM class JOIN course ON class.courseId = course.courseId WHERE class.active = 1";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                classes.Add(new ClassDTO
                                {
                                    classId = Guid.Parse(reader["classId"].ToString() ?? string.Empty),
                                    courseId = Guid.Parse(reader["courseId"].ToString() ?? string.Empty),
                                    title = reader["title"].ToString() ?? string.Empty,
                                    price = Convert.ToDecimal(reader["price"]),
                                    imgPath = reader["imgPath"].ToString() ?? string.Empty,
                                    description = reader["description"].ToString() ?? string.Empty,
                                    courseName = reader["courseName"].ToString() ?? string.Empty,
                                    active = Convert.ToBoolean(reader["active"]),
                                    ImageData = (byte[])reader["ImageData"]
                                });
                            }
                        }

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return classes;

        }

        public ClassDTO? GetById(Guid classId)
        {
            ClassDTO? clas = null;

            string query = $"SELECT * FROM class JOIN course ON class.courseId = course.courseId WHERE class.classId = @Id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@Id", classId);

                        connection.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                clas = new ClassDTO
                                {
                                    classId = Guid.Parse(reader["classId"].ToString() ?? string.Empty),
                                    courseId = Guid.Parse(reader["courseId"].ToString() ?? string.Empty),
                                    title = reader["title"].ToString() ?? string.Empty,
                                    price = Convert.ToDecimal(reader["price"]),
                                    imgPath = reader["imgPath"].ToString() ?? string.Empty,
                                    description = reader["description"].ToString() ?? string.Empty,
                                    courseName = reader["courseName"].ToString() ?? string.Empty,
                                };
                            }
                        }

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return clas;
        }

        public List<ClassDTO> GetByCategory(Guid courseId)
        {
            List<ClassDTO> clas = new List<ClassDTO>();

            string query = $"SELECT * FROM class JOIN course ON class.courseId = course.courseId WHERE class.active = 1 AND class.courseId = @Id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@Id", courseId);

                        connection.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                clas.Add(new ClassDTO
                                {
                                    classId = Guid.Parse(reader["classId"].ToString() ?? string.Empty),
                                    courseId = Guid.Parse(reader["courseId"].ToString() ?? string.Empty),
                                    title = reader["title"].ToString() ?? string.Empty,
                                    price = Convert.ToDecimal(reader["price"]),
                                    imgPath = reader["imgPath"].ToString() ?? string.Empty,
                                    description = reader["description"].ToString() ?? string.Empty,
                                    courseName = reader["courseName"].ToString() ?? string.Empty,
                                    ImageData = (byte[])reader["ImageData"]
                                });
                            }
                        }

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return clas;
        }

        public bool Insert(Class clas)
        {
            bool result = false;

            string created = clas.Created.Date.ToString("yyyy-MM-dd HH:mm:ss");
            string updated = clas.Updated.Date.ToString("yyyy-MM-dd HH:mm:ss");

            string query = $"INSERT INTO class (classId, courseId, title, price, imgPath, description, active, imageData, created, updated)" +
                $"VALUES (@Id,@courseId,@title,@price,@imgPath,@description,@active,@imageData,@created,@updated)";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@Id", clas.classId);
                        command.Parameters.AddWithValue("@courseId", clas.courseId);
                        command.Parameters.AddWithValue("@title", clas.title);
                        command.Parameters.AddWithValue("@price", clas.price);
                        command.Parameters.AddWithValue("@imgPath", clas.imgPath);
                        command.Parameters.AddWithValue("@description", clas.description);
                        command.Parameters.AddWithValue("@active", clas.active);
                        command.Parameters.AddWithValue("@imageData", clas.ImageData);
                        command.Parameters.AddWithValue("@created", created);
                        command.Parameters.AddWithValue("@updated", updated);

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

        public bool Update(Guid classId, Class clas)
        {
            bool result = false;

            string updated = clas.Updated.Date.ToString("yyyy-MM-dd HH:mm:ss");

            string query = $"UPDATE class SET title = @title, price = @price, imgPath = @imgPath, description = @description, updated = @updated" +
                $"WHERE classId = @Id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@Id", classId);
                        command.Parameters.AddWithValue("@title", clas.title);
                        command.Parameters.AddWithValue("@price", clas.price);
                        command.Parameters.AddWithValue("@imgPath", clas.imgPath);
                        command.Parameters.AddWithValue("@Iddescription", clas.description);
                        command.Parameters.AddWithValue("@updated", updated);

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

        public bool Delete(Guid classId)
        {
            bool result = false;

            string query = $"DELETE FROM class WHERE classId = @Id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@Id", classId);

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }
    }
}
