﻿using DLanguage.DTO;
using DLanguage.Models;
using Microsoft.AspNetCore.Http; // Add this namespace for HttpContext
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace DLanguage.DataAccess
{
    public class CourseDataAccess
    {
        private readonly string _connectionString;

        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor; // Add IHttpContextAccessor

        public CourseDataAccess(IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
            _httpContextAccessor = httpContextAccessor;
        }

        public List<CourseDTO> GetAll()
        {
            List<CourseDTO> courses = new List<CourseDTO>();

            string query = "SELECT * FROM course WHERE active = 1 ORDER BY courseName ASC";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CourseDTO course = new CourseDTO
                                {
                                    courseId = Guid.Parse(reader["courseId"].ToString() ?? string.Empty),
                                    courseName = reader["courseName"].ToString() ?? string.Empty,
                                    description = reader["description"].ToString() ?? string.Empty,
                                    coursePath = reader["coursePath"].ToString() ?? string.Empty,
                                };

                                // Retrieve and populate the image data from the database
                                course.ImageData = (byte[])reader["ImageData"];

                                courses.Add(course);
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return courses;
        }


        public Course? GetById(Guid courseId)
        {
            Course? course = null;

            string query = $"SELECT * FROM course WHERE courseId = @Id AND active = 1";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@Id", courseId);

                        connection.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                course = new Course
                                {
                                    courseId = Guid.Parse(reader["courseId"].ToString() ?? string.Empty),
                                    courseName = reader["courseName"].ToString() ?? string.Empty,
                                    description = reader["description"].ToString() ?? string.Empty,
                                    coursePath = reader["coursePath"].ToString() ?? string.Empty,
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                };
                            }
                        }

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return course;
        }

        public bool Insert(CourseDTO course)
        {
            bool result = false;

            Guid courseId = Guid.NewGuid();
            string created = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string updated = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string query = "INSERT INTO course (courseId, courseName, description, coursePath, active, Created, Updated, ImageData) " +
                "VALUES (@id, @courseName, @description, @coursePath, @active, @created, @updated, @imageData)";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@id", courseId);
                        command.Parameters.AddWithValue("@courseName", course.courseName);
                        command.Parameters.AddWithValue("@description", course.description);
                        command.Parameters.AddWithValue("@coursePath", course.coursePath);
                        command.Parameters.AddWithValue("@active", course.active);
                        command.Parameters.AddWithValue("@created", created);
                        command.Parameters.AddWithValue("@updated", updated);
                        command.Parameters.AddWithValue("@imageData", course.ImageData); // Insert the image data as a BLOB

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

        public bool Update(Guid courseId, Course course)
        {
            bool result = false;

            string updated = course.Updated.Date.ToString("yyyy-MM-dd HH:mm:ss");

            string query = $"UPDATE course SET courseName = @courseName, description = @description, Updated = @Updated " +
                $"WHERE courseId = @id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@id", courseId);
                        command.Parameters.AddWithValue("@courseName", course.courseName);
                        command.Parameters.AddWithValue("@description", course.description);
                        command.Parameters.AddWithValue("@Updated", updated);

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

        public bool Delete(Guid courseId)
        {
            bool result = false;

            string query = $"DELETE FROM course WHERE courseId = @id";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@id", courseId);

                        connection.Open();

                        result = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }

    }
}
