﻿using DLanguage.Models;
using MySql.Data.MySqlClient;

namespace DLanguage.DataAccess
{
    public class CustomerDataAccess
    {
        private readonly string _connectionString;
        private readonly IConfiguration _configuration;

        public CustomerDataAccess(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public bool CreateUserAccount(Customer user, UserRole userRole)
        {
            bool result = false;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                // open connection before begin transaction
                connection.Open();

                MySqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    DateTime date = DateTime.Now;

                    MySqlCommand command1 = new MySqlCommand();
                    command1.Connection = connection;
                    command1.Transaction = transaction;
                    command1.Parameters.Clear();

                    command1.CommandText = "INSERT INTO customer (customerId, customerName,email,password,isActivated,created,updated) VALUES (@customerId,@customerName,@email,@password,@isActivated,@created,@updated)";
                    command1.Parameters.AddWithValue("@customerId", user.customerId);
                    command1.Parameters.AddWithValue("@customerName", user.customerName);
                    command1.Parameters.AddWithValue("@email", user.email);
                    command1.Parameters.AddWithValue("@password", user.password);
                    command1.Parameters.AddWithValue("@isActivated", user.isActivated);
                    command1.Parameters.AddWithValue("@created", date);
                    command1.Parameters.AddWithValue("@updated", date);


                    MySqlCommand command2 = new MySqlCommand();
                    command2.Connection = connection;
                    command2.Transaction = transaction;
                    command2.Parameters.Clear();

                    command2.CommandText = "INSERT INTO userrole (customerId, role, created, updated) VALUES (@customerId, @role, @created, @updated)";
                    command2.Parameters.AddWithValue("@customerId", userRole.customerId);
                    command2.Parameters.AddWithValue("@role", userRole.role);
                    command2.Parameters.AddWithValue("@created", date);
                    command2.Parameters.AddWithValue("@updated", date);


                    var result1 = command1.ExecuteNonQuery();
                    var result2 = command2.ExecuteNonQuery();

                    transaction.Commit();

                    result = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }

            }

            return result;
        }

        public Customer? CheckUser(string email)
        {
            Customer? user = null;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    command.Connection = connection;
                    command.Parameters.Clear();

                    command.CommandText = "SELECT * From customer WHERE email = @email";
                    command.Parameters.AddWithValue("@email", email);

                    connection.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            user = new Customer
                            {
                                customerId = Guid.Parse(reader["customerId"].ToString() ?? string.Empty),
                                email = reader["email"].ToString() ?? string.Empty,
                                password = reader["password"].ToString() ?? string.Empty,
                                isActivated = Convert.ToBoolean(reader["isActivated"])
                            };
                        }
                    }

                    connection.Close();

                }
            }

            return user;
        }

        public UserRole? GetUserRole(Guid customerId)
        {
            UserRole? userRole = null;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    command.Connection = connection;
                    command.Parameters.Clear();

                    command.CommandText = "SELECT * FROM userrole WHERE customerId = @customerId";
                    command.Parameters.AddWithValue("@customerId", customerId);


                    connection.Open();

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            userRole = new UserRole
                            {
                                roleId = Convert.ToInt32(reader["roleId"]),
                                customerId = Guid.Parse(reader["customerId"].ToString() ?? string.Empty),
                                role = reader["role"].ToString() ?? string.Empty
                            };

                        }
                    }

                    connection.Close();

                }
            }

            return userRole;
        }

        public bool AcitvateUser(Guid customerId)
        {
            bool result = false;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                MySqlCommand command = new MySqlCommand();
                command.Connection = connection;
                command.Parameters.Clear();

                command.CommandText = "UPDATE customer SET isActivated = 1 WHERE customerId = @customerId";
                command.Parameters.AddWithValue("@customerId", customerId);

                try
                {
                    connection.Open();

                    result = command.ExecuteNonQuery() > 0;
                }
                catch
                {
                    throw;
                }
                finally { connection.Close(); }
            }

            return result;
        }

        public bool ResetPassword(string email, string password)
        {
            bool result = false;

            string query = "UPDATE customer SET password = @password WHERE email = @email";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    command.Connection = connection;
                    command.Parameters.Clear();

                    command.CommandText = query;

                    command.Parameters.AddWithValue("@email", email);
                    command.Parameters.AddWithValue("@password", password);

                    connection.Open();

                    result = command.ExecuteNonQuery() > 0 ? true : false;

                    connection.Close();
                }
            }

            return result;
        }

    }
}
