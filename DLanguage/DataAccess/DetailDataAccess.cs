﻿using DLanguage.DTO;
using DLanguage.Models;
using MySql.Data.MySqlClient;
using System.Data.Common;
using System.Transactions;

namespace DLanguage.DataAccess
{
    public class DetailDataAccess
    {
        private readonly string _connectionString = "server=localhost;port=3306;database=dlanguage;user=root;password=";

        public List<DetailOrderDTO> GetAll(Guid customerId)
        {
            List<DetailOrderDTO> detail = new List<DetailOrderDTO>();

            string query = "SELECT * FROM userOrder INNER JOIN detailorder ON userOrder.orderId = detailorder.orderId INNER JOIN class ON detailorder.classId = class.classId WHERE userOrder.customerId = @customerId AND userOrder.status = 0";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear(); 

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@customerId", customerId);

                        connection.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                detail.Add(new DetailOrderDTO
                                {
                                    detailId = Guid.Parse(reader["detailId"].ToString() ?? string.Empty),
                                    invoiceCode = reader["invoiceCode"].ToString() ?? string.Empty,
                                    orderDate = Convert.ToDateTime(reader["orderDate"]),
                                    totalPrice = Convert.ToDecimal(reader["totalPrice"]),
                                    classTitle = reader["classTitle"].ToString() ?? string.Empty,
                                    courseName = reader["courseName"].ToString() ?? string.Empty,
                                    scheduleDate = Convert.ToDateTime(reader["scheduleDate"]),
                                    price = Convert.ToDecimal(reader["price"]),
                                    ImageData = (byte[])reader["ImageData"]
                                });
                            }
                        }

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return detail;

        }

        public bool Keranjang(Guid classId, Guid customerId, DateOnly scheduleDate)
        {
            bool result = false;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                MySqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    string title, courseName;
                    decimal price;

                    // Take data class for Detail Order Input
                    MySqlCommand cmdGetItem = new MySqlCommand();
                    cmdGetItem.Connection = connection;
                    cmdGetItem.Transaction = transaction;
                    cmdGetItem.Parameters.Clear();

                    cmdGetItem.CommandText = "SELECT * FROM class INNER JOIN course ON class.courseId = course.courseId WHERE class.classId = @classId";
                    cmdGetItem.Parameters.AddWithValue("@classId", classId);

                    using (MySqlDataReader reader = cmdGetItem.ExecuteReader())
                    {
                        reader.Read();
                        title = reader["Title"].ToString();
                        courseName = reader["courseName"].ToString();
                        price = Convert.ToDecimal(reader["price"]);
                    }

                    MySqlCommand cmdCheckNull = new MySqlCommand();
                    cmdCheckNull.Connection = connection;
                    cmdCheckNull.Transaction = transaction;
                    cmdCheckNull.Parameters.Clear();

                    cmdCheckNull.CommandText = "SELECT COUNT(*) FROM userOrder WHERE customerId = @customerId AND status = 0";
                    cmdCheckNull.Parameters.AddWithValue("@customerId", customerId);
                    
                    if((int)(long)cmdCheckNull.ExecuteScalar() == 0)
                    {
                        DateTime date = DateTime.Now;
                        Guid newGuid = Guid.NewGuid();
                        Guid detailId = Guid.NewGuid();

                        MySqlCommand cmdOrder = new MySqlCommand();
                        cmdOrder.Connection = connection;
                        cmdOrder.Transaction = transaction;
                        cmdOrder.Parameters.Clear();

                        cmdOrder.CommandText = "INSERT INTO userOrder (orderId, customerId, invoiceCode, orderDate, totalCourse, totalPrice, status, created, updated) " +
                            "VALUES (@orderId, @customerId,'', @orderDate, 1, @totalPrice, 0, @created, @updated)";
                        cmdOrder.Parameters.AddWithValue("@orderId", newGuid);
                        cmdOrder.Parameters.AddWithValue("@customerId", customerId);
                        cmdOrder.Parameters.AddWithValue("@orderDate", date);
                        cmdOrder.Parameters.AddWithValue("@totalPrice", price);
                        cmdOrder.Parameters.AddWithValue("@created", date);
                        cmdOrder.Parameters.AddWithValue("@updated", date);
                        result = cmdOrder.ExecuteNonQuery() > 0 ? true : false;

                        MySqlCommand cmdPostDetail = new MySqlCommand();
                        cmdPostDetail.Connection = connection;
                        cmdPostDetail.Transaction = transaction;
                        cmdPostDetail.Parameters.Clear();

                        cmdPostDetail.CommandText = "INSERT INTO detailorder (detailId, orderId, classId, classTitle, courseName,scheduleDate, price, created, updated) " +
                            "VALUES (@detailId, @orderId, @classId, @classTitle, @courseName,@scheduleDate, @price, @created, @updated)";
                        cmdPostDetail.Parameters.AddWithValue("@detailId", detailId);
                        cmdPostDetail.Parameters.AddWithValue("@orderId", newGuid);
                        cmdPostDetail.Parameters.AddWithValue("@classId", classId);
                        cmdPostDetail.Parameters.AddWithValue("@classTitle", title);
                        cmdPostDetail.Parameters.AddWithValue("@courseName", courseName);
                        cmdPostDetail.Parameters.AddWithValue("@scheduleDate", scheduleDate.ToString("yyyy-MM-dd"));
                        cmdPostDetail.Parameters.AddWithValue("@price", price);
                        cmdPostDetail.Parameters.AddWithValue("@created", date);
                        cmdPostDetail.Parameters.AddWithValue("@updated", date);
                        result = cmdPostDetail.ExecuteNonQuery() > 0 ? true : false;

                    } 
                    else
                    {
                        DateTime date = DateTime.Now;
                        Guid newGuid = Guid.NewGuid();
                        Guid orderId;
                        int totalCourse;
                        decimal totalPrice;

                        // Get The OrderID
                        MySqlCommand cmdGetOrder = new MySqlCommand();
                        cmdGetOrder.Connection = connection;
                        cmdGetOrder.Transaction = transaction;
                        cmdGetOrder.Parameters.Clear();

                        cmdGetOrder.CommandText = "SELECT * FROM userOrder WHERE customerId = @customerId AND status = 0";
                        cmdGetOrder.Parameters.AddWithValue("@customerId", customerId);
                        using(MySqlDataReader read = cmdGetOrder.ExecuteReader())
                        {
                            read.Read();
                            orderId = Guid.Parse(read["orderId"].ToString() ?? string.Empty);
                            totalCourse = Convert.ToInt32(read["totalCourse"]);
                            totalPrice = Convert.ToDecimal(read["totalPrice"]);
                        }
                        
                        // Input To Detail Table
                        MySqlCommand cmdInputDetail = new MySqlCommand();
                        cmdInputDetail.Connection = connection;
                        cmdInputDetail.Transaction = transaction;
                        cmdInputDetail.Parameters.Clear();

                        cmdInputDetail.CommandText = "INSERT INTO detailorder (detailId, orderId, classId, classTitle, courseName,scheduleDate, price, created, updated) " +
                            "VALUES (@detailId, @orderId, @classId, @classTitle, @courseName,@scheduleDate, @price, @created, @updated)";
                        cmdInputDetail.Parameters.AddWithValue("@detailId", newGuid);
                        cmdInputDetail.Parameters.AddWithValue("@orderId", orderId);
                        cmdInputDetail.Parameters.AddWithValue("@classId", classId);
                        cmdInputDetail.Parameters.AddWithValue("@classTitle", title);
                        cmdInputDetail.Parameters.AddWithValue("@courseName", courseName);
                        cmdInputDetail.Parameters.AddWithValue("@scheduleDate", scheduleDate.ToString("yyyy-MM-dd"));
                        cmdInputDetail.Parameters.AddWithValue("@price", price);
                        cmdInputDetail.Parameters.AddWithValue("@created", date);
                        cmdInputDetail.Parameters.AddWithValue("@updated", date);
                        result = cmdInputDetail.ExecuteNonQuery() > 0 ? true : false;

                        // Update Table Order
                        MySqlCommand cmdUpdateOrder = new MySqlCommand();
                        cmdUpdateOrder.Connection = connection;
                        cmdUpdateOrder.Transaction = transaction;   
                        cmdUpdateOrder.Parameters.Clear();

                        cmdUpdateOrder.CommandText = "UPDATE userOrder SET orderDate = @orderDate, totalCourse = @sumCourse, totalPrice = @sumPrice, updated = @updated WHERE orderId = @orderId";
                        cmdUpdateOrder.Parameters.AddWithValue("@orderId", orderId);
                        cmdUpdateOrder.Parameters.AddWithValue("@orderDate", date);
                        cmdUpdateOrder.Parameters.AddWithValue("@sumCourse", totalCourse+1);
                        cmdUpdateOrder.Parameters.AddWithValue("@sumPrice", totalPrice + price);
                        cmdUpdateOrder.Parameters.AddWithValue("@updated", date);
                        result = cmdUpdateOrder.ExecuteNonQuery() > 0 ? true : false;
                    }

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public bool Delete(Guid detailId)
        {
            bool result = false;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    Guid orderId;
                    decimal price;

                    // Take data class for Detail Order Input
                    MySqlCommand cmdGetItem = new MySqlCommand();
                    cmdGetItem.Connection = connection;
                    cmdGetItem.Parameters.Clear();

                    cmdGetItem.CommandText = "SELECT * FROM detailOrder WHERE detailId = @detailId";
                    cmdGetItem.Parameters.AddWithValue("@detailId", detailId);

                    using(MySqlDataReader reader = cmdGetItem.ExecuteReader())
                    {
                        reader.Read();
                        orderId = Guid.Parse(reader["orderId"].ToString() ?? string.Empty);
                        price = Convert.ToDecimal(reader["price"]);
                    }

                    // Update Total Price and Course in Order
                    MySqlCommand cmdUpdateOrder = new MySqlCommand();
                    cmdUpdateOrder.Connection = connection;
                    cmdUpdateOrder.Parameters.Clear();

                    cmdUpdateOrder.CommandText = "UPDATE userorder SET totalPrice = totalPrice - @price, totalCourse = totalCourse - 1 WHERE orderId = @orderId";
                    cmdUpdateOrder.Parameters.AddWithValue("@orderId", orderId);
                    cmdUpdateOrder.Parameters.AddWithValue("@price", price);
                    result = cmdUpdateOrder.ExecuteNonQuery() > 0 ? true : false;


                    // Delete The Detail Order
                    MySqlCommand cmdDelete = new MySqlCommand();
                    cmdDelete.Connection = connection;
                    cmdDelete.Parameters.Clear();

                    cmdDelete.CommandText = "DELETE FROM detailorder WHERE detailId = @detailId";
                    cmdDelete.Parameters.AddWithValue("@detailId", detailId);
                    result = cmdDelete.ExecuteNonQuery() > 0 ? true : false;
                }
                catch
                {
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }
    }
}
