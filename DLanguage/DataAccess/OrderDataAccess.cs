﻿using DLanguage.DTO;
using DLanguage.Models;
using MySql.Data.MySqlClient;
using System.Transactions;

namespace DLanguage.DataAccess
{
    public class OrderDataAccess
    {
        private readonly string _connectionString; //"server=localhost;port=3306;database=dlanguage;user=root;password=";

        private readonly IConfiguration _configuration;

        public OrderDataAccess(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public List<Order> GetAll()
        {
            List<Order> orders = new List<Order>();

            string query = "SELECT * FROM userOrder";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    try
                    {
                        command.Connection = connection;
                        command.CommandText = query;

                        connection.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                orders.Add(new Order
                                {
                                    orderId = Guid.Parse(reader["orderId"].ToString() ?? string.Empty),
                                    customerId = Guid.Parse(reader["customerId"].ToString() ?? string.Empty),
                                    invoiceCode = reader["invoiceCode"].ToString() ?? string.Empty,
                                    orderDate = Convert.ToDateTime(reader["orderDate"]),
                                    totalCourse = Convert.ToInt32(reader["totalCourse"]),
                                    totalPrice = Convert.ToDecimal(reader["totalPrice"]),
                                    status = Convert.ToBoolean(reader["status"]),
                                    Created = Convert.ToDateTime(reader["Created"]),
                                    Updated = Convert.ToDateTime(reader["Updated"])
                                });
                            }
                        }

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return orders;

        }

        public bool Checkout(Guid customerId)
        {
            bool result = false;

            string updated = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                int totalCourse;
                decimal totalPrice;

                connection.Open();

                MySqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Read Data in Cart
                    MySqlCommand cmdGet = new MySqlCommand();
                    cmdGet.Connection = connection;
                    cmdGet.Transaction = transaction;
                    cmdGet.Parameters.Clear();

                    cmdGet.CommandText = "SELECT count(*) AS totalCourse, sum(detailOrder.price) AS totalPrice FROM detailorder INNER JOIN  userOrder ON detailorder.orderId = userOrder.orderId WHERE userorder.customerId = @customerId AND userOrder.status = 0";
                    cmdGet.Parameters.AddWithValue("@customerId", customerId);
                    using (MySqlDataReader reader = cmdGet.ExecuteReader())
                    {
                        reader.Read();
                        totalCourse = Convert.ToInt32(reader["totalCourse"]);
                        totalPrice = Convert.ToDecimal(reader["totalPrice"]);
                    }

                    // Make the invoiceCode
                    MySqlCommand cmdGetInvoice = new MySqlCommand();
                    cmdGetInvoice.Connection = connection;
                    cmdGetInvoice.Transaction = transaction;
                    cmdGetInvoice.Parameters.Clear();

                    cmdGetInvoice.CommandText = "SELECT COUNT(*) FROM userorder WHERE customerId = @customerId AND status = 1";
                    cmdGetInvoice.Parameters.AddWithValue("@customerId", customerId);
                    int lastInvoice = (int)(long)cmdGetInvoice.ExecuteScalar() + 1;
                    string noInvoice = "DLA0000";

                    // Update the Order for Checkout
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = connection;
                    command.Transaction = transaction;
                    command.Parameters.Clear();

                    command.CommandText = "UPDATE userorder SET invoiceCode = @invoiceCode, orderDate = @updated, totalCourse = @totalCourse, totalPrice = @totalPrice, status = 1, updated = @updated WHERE customerId = @customerId AND status = 0";
                    command.Parameters.AddWithValue("@customerId", customerId);
                    command.Parameters.Add(new MySqlParameter("@invoiceCode", MySqlDbType.VarChar) { Value = noInvoice + lastInvoice.ToString() ?? "" });
                    command.Parameters.AddWithValue("@orderDate", updated);
                    command.Parameters.AddWithValue("@totalCourse", totalCourse);
                    command.Parameters.AddWithValue("@totalPrice", totalPrice);
                    command.Parameters.AddWithValue("@updated", updated);

                    result = command.ExecuteNonQuery() > 0 ? true : false;

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }

            return result;
        }

        public List<OrderDTO> GetInvoiceUser(Guid customerId)
        {
            List<OrderDTO> order = new List<OrderDTO>();

            string query = "SELECT * FROM userOrder WHERE customerId = @customerId AND status = 1";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@customerId", customerId);
                        connection.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                order.Add(new OrderDTO
                                {
                                    orderId = Guid.Parse(reader["orderId"].ToString() ?? string.Empty),
                                    invoiceCode = reader["invoiceCode"].ToString() ?? string.Empty,
                                    orderDate = Convert.ToDateTime(reader["orderDate"]),
                                    totalCourse = Convert.ToInt32(reader["totalCourse"]),
                                    totalPrice = Convert.ToDecimal(reader["totalPrice"]),
                                });
                            }
                        }

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return order;

        }

        public List<DetailOrderDTO> GetDetailInvoice(Guid orderId)
        {
            List<DetailOrderDTO> order = new List<DetailOrderDTO>();

            string query = "SELECT * FROM userOrder INNER JOIN detailorder ON userOrder.orderId = detailorder.orderId WHERE userOrder.orderId = @orderId";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        command.Parameters.AddWithValue("@orderId", orderId);
                        connection.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                order.Add(new DetailOrderDTO
                                {
                                    invoiceCode = reader["invoiceCode"].ToString() ?? string.Empty,
                                    orderDate = Convert.ToDateTime(reader["orderDate"]),
                                    totalPrice = Convert.ToDecimal(reader["totalPrice"]),
                                    classTitle = reader["classTitle"].ToString() ?? string.Empty,
                                    courseName = reader["courseName"].ToString() ?? string.Empty,
                                    scheduleDate = Convert.ToDateTime(reader["scheduleDate"]),
                                    price = Convert.ToDecimal(reader["price"])
                                });
                            }
                        }

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return order;

        }

        public List<MyClassDTO> MyClass(Guid customerId)
        {
            List<MyClassDTO> myclass = new List<MyClassDTO>();

            string query = "SELECT * FROM userorder INNER JOIN detailorder ON userorder.orderId = detailorder.orderId INNER JOIN class ON detailorder.classId = class.classId WHERE userorder.customerId = @customerId AND userorder.status = 1";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    try
                    {
                        command.Connection = connection;
                        command.CommandText = query;

                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@customerId", customerId);

                        connection.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                myclass.Add(new MyClassDTO
                                {
                                    courseName = reader["courseName"].ToString() ?? string.Empty,
                                    classTitle = reader["classTitle"].ToString() ?? string.Empty,
                                    scheduleDate = DateTime.Parse(reader["scheduleDate"].ToString()),
                                    imgPath = reader["imgPath"].ToString() ?? string.Empty,
                                    ImageData = (byte[])reader["ImageData"]
                                });
                            }
                        }

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return myclass;
        }

        public List<OrderDTO> GetInvoiceAdmin()
        {
            List<OrderDTO> order = new List<OrderDTO>();

            string query = "SELECT * FROM userOrder WHERE status = 1";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    try
                    {
                        command.Connection = connection;
                        command.Parameters.Clear();

                        command.CommandText = query;
                        connection.Open();

                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                order.Add(new OrderDTO
                                {
                                    orderId = Guid.Parse(reader["orderId"].ToString() ?? string.Empty),
                                    invoiceCode = reader["invoiceCode"].ToString() ?? string.Empty,
                                    orderDate = Convert.ToDateTime(reader["orderDate"]),
                                    totalCourse = Convert.ToInt32(reader["totalCourse"]),
                                    totalPrice = Convert.ToDecimal(reader["totalPrice"]),
                                });
                            }
                        }

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return order;

        }
    }
}
