﻿namespace DLanguage.Models
{
    public class Class
    {
        public Guid classId { get; set; }
        public Guid courseId { get; set; }
        public string title { get; set; } = string.Empty;
        public decimal price { get; set; }
        public string imgPath { get; set; } = string.Empty;
        public string description { get; set; } = string.Empty;
        public bool active { get; set; }
        public byte[] ImageData { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

    }
}
