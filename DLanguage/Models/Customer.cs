﻿namespace DLanguage.Models
{
    public class Customer
    {
        public Guid customerId { get; set; }
        public string customerName { get; set; } = string.Empty;
        public string email { get; set; } = string.Empty;
        public string password { get; set; } = string.Empty;
        public bool isActivated { get; set; }
        public DateTime created { get; set; }
        public DateTime updated { get; set; }  
    }
}
