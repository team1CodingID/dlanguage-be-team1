﻿namespace DLanguage.Models
{
    public class DetailOrder
    {
        public Guid detailId { get; set; }
        public char orderId { get; set; }
        public char classId { get; set; }
        public string classTitle { get; set; } = string.Empty;
        public string courseName { get; set; } = string.Empty;
        public DateTime scheduleDate { get; set; }
        public decimal price { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
