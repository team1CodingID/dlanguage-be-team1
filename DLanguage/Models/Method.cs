﻿namespace DLanguage.Models
{
    public class Method
    {
        public Guid methodId { get; set; }
        public string methodName { get; set; } = string.Empty;
        public string methodPath { get; set; } = string.Empty;
        public bool active { get; set; }
        public byte[] ImageData { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

    }
}
