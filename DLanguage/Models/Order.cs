﻿namespace DLanguage.Models
{
    public class Order
    {
        public Guid orderId { get; set; }
        public Guid customerId { get; set; }
        public string? invoiceCode { get; set; } = string.Empty;
        public DateTime orderDate { get; set; }
        public int totalCourse { get; set; }
        public decimal totalPrice { get; set; }
        public bool? status { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
